<!-- XII RPL B_03_Amanda Faizatul Nousfaratu -->

<?php $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2>Contact Us</h2>
            <?php foreach ($alamat as $adrs) : ?>
                <ul>
                    <li><?= $adrs['whatsapp']; ?></li>
                    <li><?= $adrs['instagram']; ?></li>
                    <li><?= $adrs['facebook']; ?></li>
                </ul>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>