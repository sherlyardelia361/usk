<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: white;">
    <div class="container" style="margin:0;">
        <a class="navbar-brand" href="#"><img src="/img/logo.png"></a>
    </div>
</nav>
<!-- //div parent nya pake display flex trus align item center trus height nya 100vh -->
<div class="d-flex align-items-center pt-50">
    <div class="col-3 mx-auto" style=" height: 100vh; margin-top: 80px;">
        <h1 id="login-title" style="text-align: center;">Login</h1>
        <form action="/login/auth" method="post">
            <div class="mb-3">
                <label for="username" class="form-label">Username</label>
                <input type="text" placeholder="masukkan username" name="username" class="form-control" id="username">
            </div>
            <div class="mb-3">
                <label for="InputForPassword" class="form-label">Password</label>
                <input type="password" placeholder="masukkan password" name="password" class="form-control" id="InputForPassword">
            </div>
            <button type="submit" class="btn" style="background-color: #09A599; width: 100%; color: white;">Login</button>
            <p style="text-align: center; color: #5B5B5B; margin-top: 15px;"> Copyright © 2021 Inventool </p>
        </form>

        <?php if (session()->getFlashdata('msg')) : ?>
            <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
        <?php endif; ?>
        <?= $this->endsection(); ?>
    </div>
</div>