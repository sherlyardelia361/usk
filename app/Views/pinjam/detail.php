<!-- XII RPL B_03_Amanda Faizatul Nousfaratu -->

<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="mt-2">Detail Barang</h2>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-glutters">

                    <div class="col-md-8">
                        <div class="card-body">
                            <img src="/img/mejasiswa.jpg" width="80%" height="80%">
                            <h5 class="card-title"><?= $pinjam['barang_pinjam']; ?></h5>
                            <p class="card-text"><b>User : </b><?= $pinjam['peminjam']; ?></p>
                            <p class="card-text"><b>Jumlah Pinjam : </b><?= $pinjam['jml_pinjam']; ?></p>
                            <a href="/barang/edit/<?= $pinjam['id_pinjam']; ?>" class="btn btn-warning">Edit</a>

                            <form action="/barang/<?= $pinjam['id_pinjam']; ?>" method="POST" class="d-inline">
                                <?= csrf_field(); ?>
                                <!--  mengamankan supaya terhindar dari hacking -->
                                <input type="hidden" name="_method" value="DELETE"> <!--  name method itu spoofing nya -->
                                <button type="submit" class="btn btn-danger" onclick="return confirm('yakin ingin hapus data?');">Delete</button>
                            </form>

                            <br>
                            <br>
                            <a href="/pinjam">Kembali ke daftar peminjaman</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->endSection(); ?>