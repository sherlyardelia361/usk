<!-- XII RPL B_03_Amanda Faizatul Nousfaratu -->

<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <div class="col">


            <h1 class="mt-2">Daftar Barang</h1>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end ">
                <a href="/pinjam/create" class="btn btn-primary mt-2 b-nav">Tambah Barang</a>
            </div>
            </br>
            <?php if (session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('pesan'); ?>
                </div>
            <?php endif; ?>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Peminjam</th>
                        <th scope="col">Jumlah Pinjam</th>
                        <!-- <th scope="col">Jumlah</th> -->
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($pinjam as $p) : ?>
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td><?= $p['barang_pinjam']; ?></td>
                            <td><?= $p['peminjam']; ?></td>
                            <td><?= $p['jml_pinjam']; ?></td>
                            <td>
                                <button type="button" class="btn btn-primary">
                                    <a href="/pinjam/detail/<?= $p['id_pinjam']; ?>" style="color: white;">Detail</a>
                                </button>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>