<!-- XII RPL B_03_Amanda Faizatul Nousfaratu -->

<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-8">
            <h2 class="my-2">Form ubah barang</h2>
            <form action="/barang/update/<?= $barang['id']; ?>" method="POST" enctype="multipart/form-data">
                <!-- $barang di atas sini dari $data di method edit controller ya -->
                <?= csrf_field(); ?>
                <input type="hidden" name="slug" value="<?= $barang['slugh']; ?>">
                <input type="hidden" name="fotoLama" value="<?= $barang['foto']; ?>">
                <div class="form-group row">
                    <label for="nama_barang" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('nama_barang')) ? 'is-invalid' : ''; ?>" id="nama_barang" name="nama_barang" autofocus value="<?= (old('nama_barang')) ? old('nama_barang') : $barang['nama'] ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('nama_barang'); ?>.
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lokasi" class="col-sm-2 col-form-label">Lokasi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('lokasi')) ? 'is-invalid' : ''; ?>" id="lokasi" name="lokasi" value="<?= (old('lokasi')) ? old('lokasi') : $barang['lokasi'] ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('lokasi'); ?>.
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="foto" class="col-sm-2 col-form-label">Foto</label>
                    <div class="col-sm-2">
                        <img src="/img/<?= $barang['foto']; ?>" class="img-thumbnail img-preview">
                    </div>
                    <div class="col-sm-8">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input <?= ($validation->hasError('foto')) ? 'is-invalid' : ''; ?>" id="foto" name="foto" onchange="previewImg()">
                            <div class="invalid-feedback">
                                <?= $validation->getError('foto'); ?>.
                            </div>
                            <label class="custom-file-label" for="foto"><?= $barang['foto']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>